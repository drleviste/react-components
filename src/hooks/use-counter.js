import { useEffect, useState } from "react";

function useCounter(initialCount) {
  const [counter, setCounter] = useState(initialCount);

  useEffect(() => {
    console.log(counter);
  }, [counter]);

  const handleClick = () => {
    setCounter(counter + 1);
  };

  return {
    counter,
    handleClick,
  };
}

export default useCounter;