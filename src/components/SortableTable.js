import { GoSortAsc, GoSortDesc } from "react-icons/go";
import useSort from "../hooks/use-sort";
import Table from "./Table";

function SortableTable(props) {
  const { config, data } = props;
  const { sortBy, sortOrder, sortedData, setSortColumn } = useSort(data, config);

  const updatedConfig = config.map((column) => {
    if (!column.sortValue) {
      return column;
    }
    return {
      ...column,
      header: () => (
        <th
          onClick={() => setSortColumn(column.label)}
          className="cursor-pointer hover:bg-gray-100"
        >
          <div className="flex items-center">
            {getIcons(column.label, sortOrder, sortBy)}
            {column.label}
          </div>
        </th>
      ),
    };
  });

  return (
    <div>
      <Table {...props} config={updatedConfig} data={sortedData} />
    </div>
  );
}

function getIcons(label, sortOrder, sortBy) {
  if (label !== sortBy) {
    return (
      <div>
        <GoSortAsc />
        <GoSortDesc />
      </div>
    );
  }

  if (sortOrder === null) {
    return (
      <div>
        <GoSortAsc />
        <GoSortDesc />
      </div>
    );
  } else if (sortOrder === "asc") {
    return (
      <div>
        <GoSortAsc />
      </div>
    );
  } else if (sortOrder === "desc") {
    return (
      <div>
        <GoSortDesc />
      </div>
    );
  }
}

export default SortableTable;
