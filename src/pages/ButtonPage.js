import { GoBell } from "react-icons/go";
import Button from "../components/Button";

function ButtonPage() {
  const handleClick = () => {};

  return (
    <div>
      <div>
        <Button success rounded outline onClick={handleClick} className="mb-5">
          <GoBell />
          Click Me
        </Button>
      </div>
      <div>
        <Button primary>Play Now</Button>
      </div>
      <div>
        <Button secondary outline>
          Mute
        </Button>
      </div>
      <div>
        <Button warning>Hide Ads</Button>
      </div>
      <div>
        <Button danger rounded>
          Hide Ads
        </Button>
      </div>
    </div>
  );
};

export default ButtonPage;
