import { useState } from "react";
import Button from "../components/Button";
import Modal from "../components/Modal";

function ModalPage() {
  const [showModal, setShowModal] = useState(false);

  const handleClick = () => {
    setShowModal(true);
  };

  const handleClose = () => {
    setShowModal(false);
  };

  const actionBar = (
    <div>
      <Button onClick={handleClose} primary>
        Accept
      </Button>
    </div>
  );
  const modal = (
    <Modal onClose={handleClose} actionBar={actionBar}>
      <p>Accept this!</p>
    </Modal>
  );

  return (
    <div>
      <Button onClick={handleClick} primary>
        Open Modal!
      </Button>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin suscipit
      laoreet sapien sed blandit. Quisque sit amet placerat sem. Nam iaculis,
      sem vestibulum venenatis ullamcorper, ligula augue auctor mauris, eget
      tempus nisi erat et elit. Mauris blandit, ligula in varius semper, quam
      ligula lobortis augue, pharetra iaculis erat risus id dui. Vestibulum
      mauris turpis, sodales id lacus vel, dictum hendrerit ante. Curabitur nunc
      massa, accumsan elementum eros sed, finibus viverra sem. Nam ac magna
      lacinia, tincidunt felis a, commodo diam. Donec sit amet rutrum magna,
      consectetur finibus nibh. Etiam congue mauris diam, vitae hendrerit elit
      varius a. Phasellus sed tellus lacus. In sollicitudin lectus eget felis
      convallis, et aliquet sem imperdiet. Pellentesque auctor auctor elementum.
      Ut quis sodales arcu. Nam quis diam quis leo convallis aliquam. Nam a
      mattis metus. Etiam mattis velit in interdum vestibulum. Vestibulum ante
      ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;
      Aliquam semper odio eu diam lacinia, sit amet tempor nibh varius.
      Curabitur non rhoncus augue. Integer id facilisis massa. Donec dictum a
      diam sit amet scelerisque. Nunc et aliquam mi. Vestibulum tempus vel
      sapien nec efficitur. Quisque ut semper velit. Quisque egestas convallis
      ante et auctor. Curabitur suscipit massa non convallis convallis. Nulla
      cursus pellentesque elit quis ultrices. Etiam ut quam purus. Sed vel
      rutrum arcu. Nam felis orci, laoreet a ligula ut, finibus volutpat justo.
      Pellentesque sit amet sollicitudin lectus. Nunc tempor nunc eget felis
      condimentum, vel facilisis ex posuere. Vivamus rhoncus nisi eu suscipit
      fringilla. Nulla hendrerit lectus quam, eu vulputate sapien lacinia in.
      Aenean maximus urna id pharetra vulputate. Sed risus arcu, sagittis ac
      vestibulum vel, ultrices ut eros. Morbi bibendum efficitur urna. Aliquam
      pellentesque viverra mauris sed euismod. Aenean tincidunt ut nibh eget
      tempor. Cras iaculis vel purus et finibus. Morbi congue aliquet sem,
      tincidunt porttitor metus mattis sit amet. Phasellus sodales posuere
      magna, et auctor ex semper at. Nulla in nunc sed mi tincidunt commodo.
      Aenean hendrerit risus non nunc luctus posuere. Integer ac mattis purus.
      Donec id pulvinar enim, non laoreet lorem. Cras vestibulum tellus
      pellentesque tortor fermentum, at aliquet neque tincidunt. Duis nunc
      metus, interdum pretium pulvinar tincidunt, semper sed diam. Mauris
      varius, magna vel venenatis molestie, ipsum sapien viverra neque, vel
      faucibus eros felis a diam. Curabitur feugiat urna quam, sodales vehicula
      nulla semper at. Morbi in nisi est. Curabitur vestibulum at velit quis
      efficitur. Pellentesque nisl erat, interdum sed purus tempus, elementum
      egestas arcu. Phasellus non mollis erat, vel auctor metus. Aliquam vel
      ipsum sit amet magna ornare blandit eget eget arcu. Cras eu augue purus.
      Aliquam eget tellus nec magna pretium eleifend. Cras fermentum cursus
      diam, ac cursus odio. Sed vel pellentesque felis. Quisque ultrices ex ac
      eros consequat congue.
      {showModal && modal}
    </div>
  );
};

export default ModalPage;
