import { useState } from "react";
import Dropdown from "../components/Dropdown";

function DropdownPage() {
  const [selection, setSelection] = useState(null);

  const options = [
    { label: "Red", value: "red" },
    { label: "Pink", value: "pink" },
    { label: "Violet", value: "violet" },
  ];

  const handleSelect = (option) => {
    setSelection(option);
  };

  return (
    <Dropdown options={options} value={selection} onChange={handleSelect} />
  );
};

export default DropdownPage;
